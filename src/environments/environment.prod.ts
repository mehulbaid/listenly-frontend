export const environment = {
  production: true,
  baseUrl: 'https://app.rumbly.it/v1/api'
};
