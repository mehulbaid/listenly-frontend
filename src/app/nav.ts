import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Dashboard',
        icon: 'activity-outline',
        link: '/dashboard'
    },
    {
        title: 'Users',
        icon: 'people-outline',
        link: '/userlist'
    },
    {
        title: 'Category',
        icon: 'list-outline',
        link: '/category'
    },
    {
        title: 'Location',
        icon: 'map-outline',
        children: [
            {
                title: 'View Locations',
                icon: 'navigation-2-outline',
                link: '/location/view-location'
            },
            {
                title: 'View Default Locations',
                icon: 'navigation-outline',
                link: '/location/view-default-location'
            }
        ]
    },
    {
        title: 'Notes',
        icon: 'archive-outline',
        link: '/notes'
    },
    {
        title: 'Languages',
        icon: 'globe-outline',
        link: '/languages'
    },
];
