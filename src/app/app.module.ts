import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  NbSidebarModule,
  NbMenuModule,
  NbDatepickerModule,
  NbDialogModule,
  NbWindowModule,
  NbToastrModule,
  NbThemeModule,
  NbLayoutModule,
  NbButtonModule,
  NbIconModule,
  NbUserModule
} from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserService } from './service/user.service';
import { AuthenticationService } from './service/auth.service';
import { AuthGuard } from './service/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { LayoutComponent } from './layout/layout.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbEvaIconsModule,
    ReactiveFormsModule,
    FormsModule,
    NbLayoutModule,
    NbButtonModule,
    NbUserModule,
    NbMenuModule.forRoot(),
    NbIconModule,
    NbThemeModule.forRoot({ name: 'corporate' }),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    ToastrModule.forRoot(),
    GooglePlaceModule
  ],
  providers: [UserService, AuthenticationService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
