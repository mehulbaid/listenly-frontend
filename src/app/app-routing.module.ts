import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions, CanActivate } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './service/auth.guard';
import { LayoutComponent } from './layout/layout.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule),
    data: {
      title: 'Login Page'
    }
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./views/category/category.module').then(m => m.CategoryModule)
      },
      {
        path: 'languages',
        loadChildren: () => import('./views/languages/languages.module').then(m => m.LanguagesModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./views/location/location.module').then(m => m.LocationModule)
      },
      {
        path: 'notes',
        loadChildren: () => import('./views/notes/notes.module').then(m => m.NotesModule)
      },
      {
        path: 'userlist',
        loadChildren: () => import('./views/userlist/userlist.module').then(m => m.UserlistModule)
      }
    ]
  }
];

// const config: ExtraOptions = {
//   useHash: false
// };

@NgModule({
  imports: [RouterModule.forRoot(routes, /* config */)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
