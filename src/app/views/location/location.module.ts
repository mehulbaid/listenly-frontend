import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationRoutingModule } from './location-routing.module';
import { ViewLocationComponent } from './view-location/view-location.component';
import { ViewDefaultLocationComponent } from './view-default-location/view-default-location.component';
import { NbCardModule, NbIconModule, NbButtonModule, NbInputModule, NbDialogModule } from '@nebular/theme';
import { NgxPaginationModule } from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { EditLocationComponent } from './edit-location/edit-location.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddDefaultLocationComponent } from './add-default-location/add-default-location.component';
import { AddLocationComponent } from './add-location/add-location.component';
import { EditDefaultLocationComponent } from './edit-default-location/edit-default-location.component';

@NgModule({
  declarations: [
    ViewLocationComponent,
    ViewDefaultLocationComponent,
    EditLocationComponent,
    AddDefaultLocationComponent,
    AddLocationComponent,
    EditDefaultLocationComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    NbIconModule,
    NbButtonModule,
    NbInputModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NbDialogModule.forChild(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB7DX6C0enjwrFiQngAg1LmX2UaSbwPKv4',
      libraries: ['places']
    }),
    GooglePlaceModule,
    LocationRoutingModule
  ]
})
export class LocationModule { }
