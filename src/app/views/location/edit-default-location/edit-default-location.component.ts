import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DefaultlocationService } from '../../../service/defaultlocation.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../service/user.service';
import { NoteService } from '../../../service/notes.service';
import { forkJoin } from 'rxjs';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: 'app-edit-default-location',
  templateUrl: './edit-default-location.component.html',
  styleUrls: ['./edit-default-location.component.scss']
})
export class EditDefaultLocationComponent implements OnInit {

  EditDefaultlocationForm: FormGroup;
  imagePath: any = 'assets/cloudUpload.png';
  categoryArray: any;
  singleCategory: any;
  value: any;
  loc: any;
  selectedFile: File;
  notesPriorty: any;
  priorityArr = [];
  notesArr: any;
  cloneNotesArr: any;
  viewMode = true;
  notePriorityMap = [];
  public geoCoder;
  public options = {
    type: ['address']
    // componentRestrictions: {}
  };

  public latitude: number;
  public longitude: number;
  public zoom: number;
  @ViewChild('search', null) public searchElementRef: GooglePlaceDirective;

  constructor(
    private noteService: NoteService,
    private toastr: ToastrService,
    private defLocService: DefaultlocationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.geoCoder = new google.maps.Geocoder();
    this.value = { id: this.route.snapshot.params.id };
    this.EditDefaultlocationForm = this.formBuilder.group({
      _id: new FormControl(),
      name: new FormControl(''),
      discription: new FormControl(''),
      address: new FormControl(''),
      Latitude: new FormControl(''),
      Longitude: new FormControl(''),
      isDefault: new FormControl(''),
      loc: new FormControl(''),
      locationImage: new FormControl(''),
      notes: new FormControl(''),
      defaultCity: new FormControl('')
    });

    const location$ = this.defLocService.getDefaultLocationById(this.value.id);

    const note$ = this.noteService.getNotes();
    const observable = forkJoin([
      location$,
      note$
    ]);

    observable.subscribe(([location, note]: any) => {
      this.EditDefaultlocationForm.setValue(location);
      this.imagePath = location.locationImage;
      this.notesPriorty = location.notes;
      this.notesArr = note;
      this.cloneNotesArr = note.slice(0);
      this.zoom = 18;
      this.latitude = parseFloat(location.Latitude);
      this.longitude = parseFloat(location.Longitude);
      this.getAddress(this.latitude, this.longitude);

      this.notesPriorty.forEach(element => {
        const noteObj = this.notesArr.find((note) => element.noteId === note._id);
        const cloneIndex = this.cloneNotesArr.findIndex(ele => element.noteId === ele._id);
        this.cloneNotesArr.splice(cloneIndex, 1);
        const obj = { noteId: noteObj._id, noteName: noteObj.name, priority: element.priority };
        this.notePriorityMap.push(obj);
      });
    });

    this.getAddress(this.EditDefaultlocationForm.controls.Latitude.value, this.EditDefaultlocationForm.controls.Latitude.value);
  }

  handleAddressChange(add: Address) {
    this.latitude = add.geometry.location.lat();
    this.longitude = add.geometry.location.lng();
    this.EditDefaultlocationForm.controls.Latitude.setValue(this.latitude);
    this.EditDefaultlocationForm.controls.Longitude.setValue(this.longitude);
    this.zoom = 18;
    console.log('New Lat is:' + this.latitude);
    console.log('New Long is: ' + this.longitude);
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
          this.EditDefaultlocationForm.controls.address.setValue(results[0].formatted_address);
          // console.log(results[0].types[0]);
        } else {
          window.alert('No Results Found');
        }
      } else {
        window.alert('Geocoder Failed Due To: ' + status);
      }
    });
  }

  onSubmit() {
    const uploadData = new FormData();
    if (this.selectedFile) {
      uploadData.append('photo', this.selectedFile, this.selectedFile.name);
    } else {
      uploadData.append('photo', this.imagePath);
    }

    uploadData.append('name', this.EditDefaultlocationForm.value.name);
    uploadData.append('address', this.EditDefaultlocationForm.value.address);
    uploadData.append('Latitude', this.EditDefaultlocationForm.value.Latitude);
    uploadData.append('Longitude', this.EditDefaultlocationForm.value.Longitude);
    uploadData.append('discription', this.EditDefaultlocationForm.value.discription);
    uploadData.append('category', this.EditDefaultlocationForm.value.categoryId);
    uploadData.append('_id', this.value.id);
    uploadData.append('notes', JSON.stringify(this.notePriorityMap));
    console.log(this.value);
    this.defLocService.updateLocation(uploadData, this.value).subscribe((res: any) => {
      if (res.errorCode === 0) {
        this.toastr.success(res.errorMessage);
        this.router.navigate(['location']);
      } else {
        this.toastr.error(res.errorMessage);
        this.router.navigate(['location']);
      }
    });
  }

  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.log(file);
      this.EditDefaultlocationForm.get('photo').setValue(file);
    }
  }

  preview(files) {
    if (files.length === 0) {
      return;
    }
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    };
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  updateForm(classname) {
    const element = document.querySelector('.btnUpd');
    const reg = new RegExp('(\\s|^)' + classname + '(\\s|$)');
    element.className.replace(reg, '');
  }

  openTargetFileInput(selector) {
    const targetFileInputOpen = document.getElementById(selector);
    targetFileInputOpen.click();
  }

  addNote() {
    // tslint:disable-next-line: max-line-length
    if (((this.notePriorityMap.length) && (!this.notePriorityMap[this.notePriorityMap.length - 1] || !this.notePriorityMap[this.notePriorityMap.length - 1].noteId || !this.notePriorityMap[this.notePriorityMap.length - 1].priority))) {
      return this.toastr.error('Please fill previous entry');
    }
    if (this.notePriorityMap.length === this.notesArr.length) {
      return this.toastr.error('Notes Limit Reached');
    } else {

      try {
        let priority = this.notePriorityMap[this.notePriorityMap.length - 1].priority;
        this.priorityArr.push(++priority);
        this.notePriorityMap.push({});
      } catch (e) {
        this.priorityArr.push(1);
        this.notePriorityMap.push({});
      }
    }
  }

  remove(index) {
    const notesPriorityObj = this.notePriorityMap.splice(index, 1);
    if (notesPriorityObj && notesPriorityObj.length) {
      if (notesPriorityObj[0].noteName) {
        this.cloneNotesArr.push({ name: notesPriorityObj[0].noteName, _id: notesPriorityObj[0].noteId });
      }
      if (notesPriorityObj[0].priority) {
        this.notePriorityMap.forEach(ele => {
          if (ele.priority > notesPriorityObj[0].priority) {
            ele.priority = ele.priority - 1;
          }
          if (this.priorityArr[0] > notesPriorityObj[0].priority) {
            this.priorityArr[0] = this.priorityArr[0] - 1;
          }
        });
      }
    }
  }

  languageChange(noteId, index) {

    const noteIndex = this.cloneNotesArr.findIndex(ele => ele._id === noteId);
    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].noteId = noteId;
      this.notePriorityMap[index].noteName = this.cloneNotesArr[noteIndex].name;
    } else {
      const obj = { noteId, noteName: this.cloneNotesArr[noteIndex].name };
      this.notePriorityMap.push(obj);
    }
    this.cloneNotesArr.splice(noteIndex, 1);
  }

  priorityChange(priority, index) {

    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].priority = priority;
    } else {
      const obj = { priority };
      this.notePriorityMap.push(obj);
    }

    const priorityIndex = this.priorityArr.findIndex(ele => ele === priority);
    this.priorityArr.splice(priorityIndex, 1);
  }

}
