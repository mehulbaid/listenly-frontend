import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewLocationComponent } from './view-location/view-location.component';
import { ViewDefaultLocationComponent } from './view-default-location/view-default-location.component';
import { EditLocationComponent } from './edit-location/edit-location.component';
import { AddDefaultLocationComponent } from './add-default-location/add-default-location.component';
import { EditDefaultLocationComponent } from './edit-default-location/edit-default-location.component';
import { AddLocationComponent } from './add-location/add-location.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'view-location', pathMatch: 'full' },
  {
    path: 'view-location',
    component: ViewLocationComponent,
    data: {
      title: 'Location'
    }
  },
  {
    path: 'view-default-location',
    component: ViewDefaultLocationComponent,
    data: {
      title: 'Default Location'
    }
  },
  {
    path: 'edit-location',
    component: EditLocationComponent,
    data: {
      title: 'Edit Location'
    }
  },
  {
    path: 'add-default-location',
    component: AddDefaultLocationComponent,
    data: {
      title: 'Add Default Location'
    }
  },
  {
    path: 'edit-default-location',
    component: EditDefaultLocationComponent,
    data: {
      title: 'Edit Default Location'
    }
  },
  {
    path: 'add-location',
    component: AddLocationComponent,
    data: {
      title: 'Add Location'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule { }
