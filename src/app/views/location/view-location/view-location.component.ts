import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LocationService } from '../../../service/location.service';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'app-view-location',
  templateUrl: './view-location.component.html',
  styleUrls: ['./view-location.component.scss']
})
export class ViewLocationComponent implements OnInit {

  locationlist = [];
  p = 1;

  constructor(
    private toastr: ToastrService,
    private locationService: LocationService,
    private router: Router,
    private dialogService: NbDialogService
    ) { }

  ngOnInit() {
    this.locationService.getLocation().subscribe(res => {
      this.locationlist = res;
    });
  }

  open(addLoc: TemplateRef<any>) {
    this.dialogService.open(addLoc);
  }

  deleteLocation(user): void {
    const val = user._id;
    this.locationService.deleteLocation(val)
      .subscribe((data: any) => {
        if (data.errorCode === 0) {
          this.locationService.getLocation().subscribe(res => {
            this.locationlist = res;
          });
          this.toastr.success(data.errorMessage);
          this.router.navigate(['location']);
        } else {
          this.toastr.error(data.errorMessage);
          this.router.navigate(['location']);
        }
      }, error => {
        if (error.status === 304) {
          this.router.navigate(['location']);
        }
      });
  }

  editLocation(user): void {
    const id = user._id;
    this.router.navigate(['location/edit-location', { id }]);
  }

}
