import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DefaultlocationService } from '../../../service/defaultlocation.service';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'app-view-default-location',
  templateUrl: './view-default-location.component.html',
  styleUrls: ['./view-default-location.component.scss']
})
export class ViewDefaultLocationComponent implements OnInit {

  defaultLocList = [];
  p = 1;

  constructor(
    private toastr: ToastrService,
    private defLocService: DefaultlocationService,
    private router: Router,
    private dialogService: NbDialogService
  ) { }

  ngOnInit() {
    this.defLocService.getDefaultLocation().subscribe(defLoc => {
      this.defaultLocList = defLoc;
    });
  }

  open(addLoc: TemplateRef<any>) {
    this.dialogService.open(addLoc);
  }

  deleteLocation(user): void {
    const val = user._id;
    this.defLocService.deleteLocation(val)
      .subscribe((data: any) => {
        if (data.errorCode === 0) {
          this.defLocService.getDefaultLocation().subscribe(res => {
            this.defaultLocList = res;
          });
          this.toastr.success(data.errorMessage);
          this.router.navigate(['location/view-default-location']);
        } else {
          this.toastr.error(data.errorMessage);
          this.router.navigate(['location/view-default-location']);
        }
      }, error => {
        if (error.status === 304) {
          this.router.navigate(['location/view-default-location']);
        }
      });
  }

  editLocation(user): void {
    const id = user._id;
    this.router.navigate(['location/edit-default-location', { id }]);
  }

}
