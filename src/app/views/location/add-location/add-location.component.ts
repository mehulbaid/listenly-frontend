import { Component, OnInit, NgZone, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LocationService } from '../../../service/location.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NoteService } from '../../../service/notes.service';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';


@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss']
})
export class AddLocationComponent implements OnInit {
  categoryArray = [];
  locationForm: FormGroup;
  selectedFile: File;
  imagePath: any = 'assets/cloudUpload.png';
  notesArr: any;
  collection = [];
  priorityArr = [];
  notePriorityMap = [];
  cloneNotesArr: any;
  searchResults = [];
  public geoCoder;
  public options = {
    type: ['address']
    // componentRestrictions: {}
  };

  public latitude: number;
  public longitude: number;
  public zoom = 18;

  @ViewChild('search', { static: false }) public searchElementRef: GooglePlaceDirective;
  constructor(
    private noteService: NoteService,
    private router: Router,
    private toastr: ToastrService,
    private category: UserService,
    private formBuilder: FormBuilder,
    private location: LocationService
    ) {
      if (navigator) {
        navigator.geolocation.getCurrentPosition(pos => {
          this.latitude = pos.coords.latitude;
          this.longitude = pos.coords.longitude;
          console.log('Lat:' + this.latitude);
          console.log('Long:' + this.longitude);
          this.zoom = 18;
        });
      }
     }

  ngOnInit() {
    this.locationForm = this.formBuilder.group({
      name: ['', Validators.required],
      discription: [''],
      address: ['', Validators.required],
      Latitude: [''],
      Longitude: [''],
      category: ['', Validators.required],
      isDefault: ['']
    });

    this.locationForm.controls.isDefault.setValue('false');

    this.category.getCategory().subscribe((data: any) => {
      this.categoryArray = data;
      console.log(data);
    });

    this.noteService.getNotes().subscribe((data: any) => {
      this.notesArr = data;
      this.cloneNotesArr = data.slice(0);
      console.log('clone', this.cloneNotesArr);
      if (this.notesArr) {
        this.collection.push(1);
        this.priorityArr.push(1);
      }
    });

    this.zoom = 18;
    this.latitude = 28.6139391;
    this.longitude = 77.20902120000005;
    this.geoCoder = new google.maps.Geocoder();
    this.getAddress(this.latitude, this.longitude);

    // set current position
    this.setCurrentPosition();

    console.log(this.searchResults);
  }

  public handleAddressChange(add: Address) {
    this.latitude = add.geometry.location.lat();
    this.longitude = add.geometry.location.lng();
    this.locationForm.controls.Latitude.setValue(this.latitude);
    this.locationForm.controls.Longitude.setValue(this.longitude);
    this.zoom = 18;
    this.getAddress(this.latitude, this.longitude);
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng:  longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
        } else {
          window.alert('No Results Found');
        }
      } else {
        window.alert('Geocoder Failed Due To: ' + status);
      }
    });
  }


  onSubmit(t) {
    // console.log('this.selectedFile', this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('photo', this.selectedFile, this.selectedFile.name);
    uploadData.append('address', this.locationForm.controls.address.value);
    uploadData.append('name', this.locationForm.controls.name.value);
    uploadData.append('Latitude', this.locationForm.controls.Latitude.value);
    uploadData.append('Longitude', this.locationForm.controls.Longitude.value);
    uploadData.append('discription', this.locationForm.controls.discription.value);
    uploadData.append('category', this.locationForm.controls.category.value);
    uploadData.append('notes', JSON.stringify(this.notePriorityMap));
    uploadData.append('isDefault', this.locationForm.controls.isDefault.value);
    uploadData.forEach(ele => {
      console.log('\nformData is ' + this.selectedFile + '\n');
    });
    this.location.createLocation(uploadData).subscribe((res: any) => {
      if (res.errorCode === 0) {
        this.toastr.success(res.errorMessage);
        this.router.navigate(['location']);
      } else {
        this.toastr.error(res.errorMessage);
        this.router.navigate(['location']);
      }
    });

  }


  getErrorMessage(control) {

    return this.locationForm.controls[control].hasError('required')
      ? 'Please enter the required field'
      : this.locationForm.controls[control].hasError('email')
        ? 'Please enter a valid email'
        : this.locationForm.controls[control].hasError('minlength')
          ? 'Password should be of min 8 charachters'
          : this.locationForm.controls[control].hasError('pattern')
            ? 'Price shoud be valid number'
            : null;
  }
  getControl(control) {
    return this.locationForm.controls[control].touched && !this.locationForm.controls[control].valid;
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }
  preview(files) {
    if (files.length === 0) {
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    // tslint:disable-next-line: variable-name
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    };
  }
  private setCurrentPosition() {
    // debugger
    if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 18;
      this.getAddress(this.latitude, this.longitude);
    });
   }
  }
  openTargetFileInput(selector) {
    const targetFileInputOpen = document.getElementById(selector);
    targetFileInputOpen.click();
  }

  languageChange(noteId, index) {

    const noteIndex = this.cloneNotesArr.findIndex(ele => ele._id === noteId);
    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].noteId = noteId;
      this.notePriorityMap[index].noteName = this.cloneNotesArr[noteIndex].name;
    } else {
      const obj = { noteId, noteName: this.cloneNotesArr[noteIndex].name };
      this.notePriorityMap.push(obj);
    }
    this.cloneNotesArr.splice(noteIndex, 1);
  }
  priorityChange(priority, index) {
    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].priority = priority;
    } else {
      const obj = { priority };
      this.notePriorityMap.push(obj);
    }

    const priorityIndex = this.priorityArr.findIndex(ele => ele === priority);
    this.priorityArr.splice(priorityIndex, 1);

  }
  remove(index) {
    this.collection.splice(index, 1);
    const notesPriorityObj = this.notePriorityMap.splice(index, 1);
    if (notesPriorityObj && notesPriorityObj.length) {
      if (notesPriorityObj[0].noteName) {
        this.cloneNotesArr.push({ name: notesPriorityObj[0].noteName, _id: notesPriorityObj[0].noteId });
      }

      if (notesPriorityObj[0].priority) {
        this.notePriorityMap.forEach(ele => {
          if (ele.priority > notesPriorityObj[0].priority) {
            ele.priority = ele.priority - 1;
          }
        });
      }

      if (!notesPriorityObj[0].priority) {
        this.priorityArr.pop();
      }
    }
  }
  addNote() {
    // tslint:disable-next-line: max-line-length
    if ((this.collection.length) && (!this.notePriorityMap[this.collection.length - 1] || !this.notePriorityMap[this.collection.length - 1].noteId || !this.notePriorityMap[this.collection.length - 1].priority)) {
      return this.toastr.error('Please fill previous entry');
    }
    if (this.collection.length === this.notesArr.length) {
      return this.toastr.error('Notes Limit Reached');
    } else {
      this.collection.push(1);
      const length = this.collection.length;
      this.priorityArr.push(length);
    }
  }
}
