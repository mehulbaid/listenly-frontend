import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DefaultlocationService } from '../../../service/defaultlocation.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NoteService } from '../../../service/notes.service';
import { MouseEvent } from '@agm/core';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';


@Component({
  selector: 'app-add-default-location',
  templateUrl: './add-default-location.component.html',
  styleUrls: ['./add-default-location.component.scss']
})
export class AddDefaultLocationComponent implements OnInit {

  defaultLocationForm: FormGroup;
  selectedFile: File;
  imagePath: any = 'assets/cloudUpload.png';
  notesArr: any;
  collection = [];
  priorityArr = [];
  notePriorityMap = [];
  cloneNotesArr: any;
  searchResults = [];
  public geoCoder;
  public options = {
    type: ['address']
    // componentRestrictions: {}
  };

  public latitude: number;
  public longitude: number;
  public zoom: number = 18;

  @ViewChild('search', { static: false }) public searchElementRef: GooglePlaceDirective;

  constructor(
    private noteService: NoteService,
    private router: Router,
    private toastr: ToastrService,
    private category: UserService,
    private formBuilder: FormBuilder,
    private location: DefaultlocationService
  ) {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.latitude = pos.coords.latitude;
        this.longitude = pos.coords.longitude;
        this.zoom = 18;
      });
    }
  }

  ngOnInit() {
    this.defaultLocationForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      discription: new FormControl(''),
      address: new FormControl(''),
      Latitude: new FormControl(''),
      Longitude: new FormControl(''),
      isDefault: new FormControl(''),
      defaultCity: new FormControl('', Validators.required)
    });

    this.defaultLocationForm.controls.isDefault.setValue('true');

    this.noteService.getNotes().subscribe((data: any) => {
      this.notesArr = data;
      this.cloneNotesArr = data.slice(0);

      if (this.notesArr) {
        this.collection.push(1);
        this.priorityArr.push(1);
      }
    });

    this.zoom = 18;
    this.latitude = 28.6139391;
    this.longitude = 77.20902120000005;
    this.geoCoder = new google.maps.Geocoder();
    this.getAddress(this.latitude, this.longitude);

    // this.setCurrentPositon();
  }

  public handleAddressChange(add: Address) {
    this.latitude = add.geometry.location.lat();
    this.longitude = add.geometry.location.lng();
    this.defaultLocationForm.controls.Latitude.setValue(this.latitude);
    this.defaultLocationForm.controls.Longitude.setValue(this.longitude);
    this.zoom = 18;
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.defaultLocationForm.controls.Latitude.setValue($event.coords.lat);
    this.defaultLocationForm.controls.Longitude.setValue($event.coords.lng);
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 18;
        } else {
          window.alert('No Results Found');
        }
      } else {
        window.alert('Geocoder Failed Due To: ' + status);
      }
    });
  }

  onSubmit() {
    const uploadData = new FormData();
    uploadData.append('photo', this.selectedFile, this.selectedFile.name);
    uploadData.append('address', this.defaultLocationForm.controls.address.value);
    uploadData.append('name', this.defaultLocationForm.controls.name.value);
    uploadData.append('Latitude', this.defaultLocationForm.controls.Latitude.value);
    uploadData.append('Longitude', this.defaultLocationForm.controls.Longitude.value);
    uploadData.append('discription', this.defaultLocationForm.controls.discription.value);
    uploadData.append('notes', JSON.stringify(this.notePriorityMap));
    uploadData.append('isDefault', this.defaultLocationForm.controls.isDefault.value);
    uploadData.append('defaultCity', this.defaultLocationForm.controls.defaultCity.value);
    uploadData.forEach(ele => {
      console.log('\nformData is ' + this.selectedFile + '\n');
    });
    this.location.createDefaultLocation(uploadData).subscribe((res: any) => {
      if (res.errorCode === 0) {
        this.toastr.success(res.errorMessage);
        this.router.navigate(['location/view-default-location']);
      } else {
        this.toastr.error(res.errorMessage);
        this.router.navigate(['location/view-default-location']);
      }
    });
  }

  getErrorMessage(control) {
    return this.defaultLocationForm.controls[control].hasError('required')
      ? 'Please enter the required field'
      : this.defaultLocationForm.controls[control].hasError('email')
        ? 'Please enter a valid email'
        : this.defaultLocationForm.controls[control].hasError('minlength')
          ? 'Password should be of min 8 charachters'
          : this.defaultLocationForm.controls[control].hasError('pattern')
            ? 'Price shoud be valid number'
            : null;
  }

  getControl(control) {
    return this.defaultLocationForm.controls[control].touched && !this.defaultLocationForm.controls[control].valid;
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }
  preview(files) {
    if (files.length === 0) {
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    // tslint:disable-next-line: variable-name
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    };
  }

  private setCurrentPosition() {
    // debugger
    if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.zoom = 18;
      this.getAddress(this.latitude, this.longitude);
    });
   }
  }
  openTargetFileInput(selector) {
    const targetFileInputOpen = document.getElementById(selector);
    targetFileInputOpen.click();
  }

  languageChange(noteId, index) {

    const noteIndex = this.cloneNotesArr.findIndex(ele => ele._id === noteId);
    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].noteId = noteId;
      this.notePriorityMap[index].noteName = this.cloneNotesArr[noteIndex].name;
    } else {
      const obj = { noteId, noteName: this.cloneNotesArr[noteIndex].name };
      this.notePriorityMap.push(obj);
    }
    this.cloneNotesArr.splice(noteIndex, 1);
  }
  priorityChange(priority, index) {
    if (this.notePriorityMap[index]) {
      this.notePriorityMap[index].priority = priority;
    } else {
      const obj = { priority };
      this.notePriorityMap.push(obj);
    }

    const priorityIndex = this.priorityArr.findIndex(ele => ele === priority);
    this.priorityArr.splice(priorityIndex, 1);

  }
  remove(index) {
    this.collection.splice(index, 1);
    const notesPriorityObj = this.notePriorityMap.splice(index, 1);
    if (notesPriorityObj && notesPriorityObj.length) {
      if (notesPriorityObj[0].noteName) {
        this.cloneNotesArr.push({ name: notesPriorityObj[0].noteName, _id: notesPriorityObj[0].noteId });
      }

      if (notesPriorityObj[0].priority) {
        this.notePriorityMap.forEach(ele => {
          if (ele.priority > notesPriorityObj[0].priority) {
            ele.priority = ele.priority - 1;
          }
        });
      }

      if (!notesPriorityObj[0].priority) {
        this.priorityArr.pop();
      }
    }
  }
  addNote() {
    // tslint:disable-next-line: max-line-length
    if ((this.collection.length) && (!this.notePriorityMap[this.collection.length - 1] || !this.notePriorityMap[this.collection.length - 1].noteId || !this.notePriorityMap[this.collection.length - 1].priority)) {
      return this.toastr.error('Please fill previous entry');
    }
    if (this.collection.length === this.notesArr.length) {
      return this.toastr.error('Notes Limit Reached');
    } else {
      this.collection.push(1);
      const length = this.collection.length;
      this.priorityArr.push(length);
    }
  }
}
