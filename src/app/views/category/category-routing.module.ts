import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCategoryComponent } from './view-category/view-category.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';


const routes: Routes = [
  {
    path: '', component: ViewCategoryComponent,
    data: {
      title: 'Category'
    },
  },
  {
    path: 'add-category', component: AddCategoryComponent,
    data: {
      title: 'Add Category'
    },
  },
  {
    path: 'edit-category', component: EditCategoryComponent,
    data: {
      title: 'edit Category'
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
