import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss']
})
export class ViewCategoryComponent implements OnInit {

  categoryList = [];
  p = 1;

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getCategory().subscribe(res => {
      this.categoryList = res;
    });
  }

  deleteCategory(user): void {
    if (confirm('Are you sure to delete category ' + user.name + '?')) {
      const val = user._id;
      this.userService.deleteCategory(val)
        .subscribe((data: any) => {
          if (data.errorCode === 0) {
            this.userService.getCategory().subscribe(res => {
              this.categoryList = res;
            });
            this.toastr.success(data.errorMessage);
            this.router.navigate(['category']);
          } else {
            this.toastr.error(data.errorMessage);
            this.router.navigate(['category']);
          }
        },
          error => {
            if (error.status === 304) {
              // alert(" You Can Delete  only Your Users ");
              this.router.navigate(['category']);
            }
          }
        );
     }
  }

  editCategory(user): void {
    let id = user._id;
    this.router.navigate(['category/edit-category', { id }]);
  }

}
