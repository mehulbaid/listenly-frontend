import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { fbind } from 'q';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  category: FormGroup;
  // uploadForm: FormGroup;
  selectedFile: File;
  imagePath: any = 'assets/cloudUpload.png';
  // tslint:disable-next-line: no-shadowed-variable
  constructor(private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router, private UserService: UserService) { }

  ngOnInit() {
    this.category = this.formBuilder.group({
      category: ['', Validators.required],
      profile: [''],
    });
  }

  onSubmit() {
    const uploadData = new FormData();
    if ( this.selectedFile === undefined || this.selectedFile == null) {
       this.toastr.error('please add category image');
    } else {
      uploadData.append('photo', this.selectedFile, this.selectedFile.name);
      uploadData.append('name', this.category.value.category);
      //  console.log(this.category.value.category)
      this.UserService.addCategory(uploadData).subscribe((res: any) => {
        if (res.errorCode === 0) {
          this.toastr.success(res.errorMessage);
          this.router.navigate(['category']);
        } else {
          this.toastr.error(res.errorMessage);
          this.router.navigate(['category']);
        }

      },
      );
    }
  }

  preview(files) {
    if (files.length === 0) {
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    // tslint:disable-next-line: variable-name
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    };
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  openTargetFileInput(selector) {
    const targetFileInputOpen = document.getElementById(selector);
    targetFileInputOpen.click();
  }


}
