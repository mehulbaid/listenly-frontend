import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  editUser: FormGroup;
  user;
  value: any;
  imagePath: any = 'assets/cloudUpload.png';
  selectedFile: File;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
    ) { }

  ngOnInit() {
    this.value = { id: this.route.snapshot.params.id };
    // console.log(this.route.snapshot.params['id'])

    this.userService.getCategoryById(this.value.id)
      .subscribe((data: any) => {
        this.editUser.setValue(data);
        this.imagePath = data.photo;
      });

    this.editUser = this.formBuilder.group({
      _id: [],
      name: ['', Validators.required],
      photo: [''],
    });

  }

  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.log(file);
      this.editUser.get('photo').setValue(file);
    }
  }
  preview(files) {
    if (files.length === 0) {
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    };
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('photo', this.editUser.get('photo').value);
    formData.append('name', this.editUser.get('name').value);
    formData.append('_id', this.value.id);


    //  console.log(this.editUser.get('photo').value,this.editUser.get('name').value,this.value.id,this.editUser.value)
    this.userService.updateCategory(formData, this.value).subscribe((data: any) => {
      if (data.errorCode === 0) {
        this.toastr.success(data.errorMessage);
        this.router.navigate(['category']);
      } else {
        this.toastr.error(data.errorMessage);
        this.router.navigate(['edit-category']);
      }

    });
  }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }


  openTargetFileInput(selector) {
    const targetFileInputOpen = document.getElementById(selector);
    targetFileInputOpen.click();
  }
}

