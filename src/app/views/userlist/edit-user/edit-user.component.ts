import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  editUser: FormGroup;
  user;
  value: any;
  // dob:null;
  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.value = { id: this.route.snapshot.params.id };
    this.editUser = this.formBuilder.group({
      _id: [],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      username: ['', Validators.required],
    });
    this.userService.getUserById(this.value.id)
      .subscribe(data => {
        this.editUser.setValue(data);
      });
  }

  getErrorMessage(control) {
    return this.editUser.controls[control].hasError('required')
      ? 'Please enter the required field'
      : this.editUser.controls[control].hasError('email')
        ? 'Please enter a valid email'
        : this.editUser.controls[control].hasError('minlength')
          ? 'Password should be of min 8 charachters'
          : this.editUser.controls[control].hasError('pattern')
            ? 'Date should be in format dd/mm/yyyy'
            : null;
  }
  getControl(control) {
    return this.editUser.controls[control].touched && !this.editUser.controls[control].valid;
  }

  onSubmit(t) {
    this.userService.updateUser(this.editUser.value)
      .subscribe((data: any) => {
        if (data) {
          this.toastr.success(data.errorMessage);
          this.router.navigate(['userlist']);
        } else {
          this.toastr.error(data.errorMessage);
          this.router.navigate(['userlist/edit-user']);
        }

      });
  }
}
