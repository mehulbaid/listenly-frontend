import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
  userlist = [];
  p = 1;
  // users: User[];
  constructor(private toastr: ToastrService, private userservice: UserService, private router: Router, ) { }

  ngOnInit() {
    this.userservice.getUsers().subscribe(res => {
      this.userlist = res;
    });
  }

  deleteUser(user): void {
    if (confirm('Are you sure you to delete the user ' + user.username + ' ?')) {
    const val = user._id;
    this.userservice.deleteUser(val)
      .subscribe(data => {
          if (data.errorCode === 0) {
            this.userservice.getUsers().subscribe(res => {
              this.userlist = res;
            });
            this.toastr.success(data.errorMessage);
            this.router.navigate(['userlist']);
          } else {
            this.toastr.error(data.errorMessage);
            this.router.navigate(['userlist']);
          }
        },
      );
    }
  }

  editUser(user): void {
    const id = user._id;
    this.router.navigate(['userlist/edit-user', { id }]);

  }

}
