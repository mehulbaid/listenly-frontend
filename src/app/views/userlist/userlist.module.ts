import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserlistRoutingModule } from './userlist-routing.module';
import { UserlistComponent } from './userlist.component';
import { NbCardModule, NbIconModule, NbButtonModule, NbInputModule, NbDialogModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { EditUserComponent } from './edit-user/edit-user.component';


@NgModule({
  declarations: [UserlistComponent, EditUserComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbIconModule,
    NbButtonModule,
    NbInputModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NbDialogModule.forChild(),
    UserlistRoutingModule
  ]
})
export class UserlistModule { }
