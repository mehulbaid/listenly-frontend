import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { Routes, RouterModule} from '@angular/router';
import { NbToastrModule, NbCardModule, NbInputModule, NbButtonModule, NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';

const loginRoute: Routes = [
  { path: '', component: LoginComponent }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    NbToastrModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    NbThemeModule.forRoot({ name: 'corporate' }),
    NbLayoutModule,
    ReactiveFormsModule,
    RouterModule.forChild(loginRoute)
  ]
})
export class LoginModule { }
