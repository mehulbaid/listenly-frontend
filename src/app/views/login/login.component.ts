import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth.service';
import { User } from '../../model/user.model';
import { NbToastrService } from '@nebular/theme';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: FormGroup;
  submitted: boolean = false;
  invalidLogin: boolean = false;
  btnDisabled: boolean = false;

  constructor(
    private toastr: ToastrService, private formBuilder: FormBuilder,
    private router: Router, private authService: AuthenticationService
  ) { }

  onSubmit() {
    this.btnDisabled = true;
    this.submitted = true;

    if (this.login.invalid) {
      return;
    }

    this.authService.login(this.Email.value, this.Pass.value).subscribe(res => {

      if (res.errorCode === 0) {
        this.toastr.success(res.errorMessage, 'SUCCESS');
        this.router.navigate(['dashboard']);
      } else {
        this.toastr.error(res.errorMessage, 'ERROR');
        this.router.navigate(['login']);
      }
    });
  }

  ngOnInit() {
    this.login = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(/^[\w.]+@[\w]+?(\.[a-zA-Z]{2,3}){1,3}$/)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  get Email() {
    return this.login.controls.email;
  }

  get Pass() {
    return this.login.controls.password;
  }

  // showToast(msg, status) {
  //   this.toastr.show(msg, status, { status });
  // }

}
