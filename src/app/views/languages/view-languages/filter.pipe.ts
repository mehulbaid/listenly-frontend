import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(langArray: any[], field: string, searchText: string): any[] {
    if (!langArray) { return []; }
    if (!field || !searchText) { return langArray; }

    return langArray.filter( singleItem => {
      singleItem[field].toLowerCase().includes();
    });
  }
}
