import { Component, OnInit, TemplateRef } from '@angular/core';
import { LanguageService } from '../../../service/language.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Lang } from './lang';
import { NbDialogService } from '@nebular/theme';
import { AddLanguagesComponent } from '../add-languages/add-languages.component';

@Component({
  selector: 'app-view-languages',
  templateUrl: './view-languages.component.html',
  styleUrls: ['./view-languages.component.scss']
})
export class ViewLanguagesComponent implements OnInit {

  langArray: Lang[] = [];
  searchText: string;

  tableHeads = ['#', 'Language Id', 'Language Name', 'Language Code', 'Actions'];

  constructor(
    private langServ: LanguageService,
    private router: Router,
    private toastr: ToastrService,
    private dialogServ: NbDialogService
    ) { }

  open(dialog: TemplateRef<any>) {
    this.dialogServ.open(dialog);
  }

  cancelAdd() {
    this.toastr.warning('The Language Adding Was Cancelled!!', 'Language Adding Cancelled!!');
  }

  listLanguages() {
    this.langServ.getLanguage().subscribe((lang: any) => {
      this.langArray = lang;
    });
  }

  // tslint:disable-next-line: variable-name
  removeLanguage(_id: string) {
    this.langServ.deleteLanguage(_id).subscribe(res => {
      this.toastr.success('Language Deleted!');
    }, (err) => {
      console.log(err);
    });
    this.router.navigateByUrl('languages/viewlanguages');
  }

  ngOnInit() {
    this.listLanguages();
  }

}
