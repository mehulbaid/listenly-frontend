import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewLanguagesComponent } from './view-languages/view-languages.component';


const routes: Routes = [
  { path: '', component: ViewLanguagesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguagesRoutingModule { }
