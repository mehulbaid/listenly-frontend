import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LanguagesRoutingModule } from './languages-routing.module';
import { ViewLanguagesComponent } from './view-languages/view-languages.component';
import { NbCardModule, NbButtonModule, NbInputModule, NbDialogModule, NbIconModule } from '@nebular/theme';
import { FilterPipe } from './view-languages/filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddLanguagesComponent } from './add-languages/add-languages.component';


@NgModule({
  declarations: [ViewLanguagesComponent, FilterPipe, AddLanguagesComponent],
  imports: [
    CommonModule,
    LanguagesRoutingModule,
    NbCardModule,
    NbDialogModule.forChild(),
    FormsModule,
    NbIconModule,
    ReactiveFormsModule,
    NbButtonModule,
    NbInputModule
  ]
})
export class LanguagesModule { }
