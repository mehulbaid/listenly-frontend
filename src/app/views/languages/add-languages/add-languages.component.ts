import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../service/language.service';

@Component({
  selector: 'app-add-languages',
  templateUrl: './add-languages.component.html',
  styleUrls: ['./add-languages.component.scss']
})
export class AddLanguagesComponent implements OnInit {

  langName = new FormControl('', Validators.required);
  langCode = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]);
  langError = false;

  constructor(private router: Router, private toastr: ToastrService, private langServ: LanguageService) { }

  // cancelAdd() {
  //   this.router.navigate(['languages']);
  //   this.toastr.warning('The Language Adding Was Cancelled!!', 'Language Adding Cancelled!!');
  // }

  addLanguage() {
    this.langServ.addLanguage(this.langName.value, this.langCode.value).subscribe((data: any) => {
      this.toastr.success('Added Successfully');
      console.log(data);
    });
    this.router.navigate(['languages']);
  }

  ngOnInit() {
  }

}
