import { Injectable } from '@angular/core';
import { Observable,of  } from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {User} from '../model/user.model';
import {environment} from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'responseType': 'json'
  });
  let options = {headers:headers};

  }
  
  // environment.baseUrl: string = 'http://localhost:4000/v1/api';
  // baseUrl: string ='http://13.232.198.112:4000/v1/api';

  getUsers() {
    return this.http.get<User[]>(environment.baseUrl+'/userlist');
  }

  getUserById(id: number) {
    return this.http.get<User>(environment.baseUrl + '/singleuser/' + id);
  }

  createUser(user: User) {
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.post(environment.baseUrl+'/signup', user,{headers:headers_object});
  }

  updateUser(user: User) {
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.patch(environment.baseUrl + '/update/' + user._id, user,{headers:headers_object});
  }

  deleteUser(id: string) {
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.delete<any>(environment.baseUrl + '/user/' + id,{headers:headers_object});
  }

  addCategory(user) {
    console.log(user)
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.post(environment.baseUrl+'/add-category', user,{headers:headers_object});
  }

  getCategory() {
    return this.http.get<User[]>(environment.baseUrl+'/categoryList');
  }
  getLanguages() {
    return this.http.get<User[]>(environment.baseUrl+'/getlanguage');
  }
  updateCategory(user,iddata) {
    console.log("adsj",iddata)
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.patch(environment.baseUrl + '/edit-category/' + iddata.id, user,{headers:headers_object});
  }
  deleteCategory(id: string) {
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.delete<any>(environment.baseUrl + '/delete-category/' + id,{headers:headers_object});
  }
  getCategoryById(id: number) {
    return this.http.get<User>(environment.baseUrl + '/singleCategory/' + id);
  }
}