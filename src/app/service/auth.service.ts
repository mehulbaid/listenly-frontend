import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment'

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {
   
  }
  // baseUrl: string = 'http://localhost:4000';
  // baseUrl: string ='http://13.232.198.112:4000';
  
  login(email: string, password: string) {
    console.log(environment.baseUrl)
        return this.http.post<any>(environment.baseUrl+'/'+'user-login', {email: email, password: password})
      .pipe(map(user => {
      console.log(user)
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }
  
  signup(value){
    return this.http.post<any>(environment.baseUrl+'/'+'signup', value);
  }
}
