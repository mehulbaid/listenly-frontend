import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../model/user.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DefaultlocationService {

  constructor(private http: HttpClient) { }

  // a = JSON.parse(localStorage.getItem('currentUser'));

  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  options = { headers: this.headers };

  // environment.baseUrl: string = 'http://localhost:4000/v1/api';
  // baseUrl: string ='https://app.rumbly.it/v1/api';

  getDefaultLocation() {
    return this.http.get<User[]>(environment.baseUrl + '/default-location');
  }

  getDefaultLocationById(id: number) {
    return this.http.get<User[]>(environment.baseUrl + '/default-location/' + id);
  }

  createDefaultLocation(user) {
    console.log(user);
    const a = JSON.parse(localStorage.getItem('currentUser'));
    const httpOptions = new HttpHeaders().set('Authorization', 'Bearer ' + a.token );
    return this.http.post(environment.baseUrl + '/default-location', user, { headers: httpOptions});
  }

  updateLocation(user, iddata) {
    const a = JSON.parse(localStorage.getItem('currentUser'));
    const httpOptions = new HttpHeaders().set('Authorization', 'Bearer ' + a.token );
    return this.http.patch(environment.baseUrl + '/default-location/' + iddata.id, user, { headers: httpOptions});
  }

  deleteLocation(id: string) {
    const a = JSON.parse(localStorage.getItem('currentUser'));
    const httpOptions = new HttpHeaders().set('Authorization', 'Bearer ' + a.token );
    return this.http.delete<any>(environment.baseUrl + '/default-location/' + id, { headers: httpOptions});
  }
}
