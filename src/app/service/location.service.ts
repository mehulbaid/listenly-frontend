import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor(private http: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'responseType': 'json'
    });
    let options = { headers: headers };

  }

  // environment.baseUrl: string = 'http://localhost:4000/v1/api';
  // baseUrl: string ='http://13.232.198.112:4000/v1/api';

  getLocation() {
    return this.http.get<User[]>(environment.baseUrl + '/location');
  }

  getLocationById(id: number) {
    return this.http.get<User>(environment.baseUrl + '/location/' + id);
  }

  createLocation(user) {
    console.log(user)
    let a = JSON.parse(localStorage.getItem('currentUser'));
    let headers_object = new HttpHeaders().set('Authorization', 'Bearer ' + a.token);
    return this.http.post(environment.baseUrl + '/location', user, { headers: headers_object });
  }

  updateLocation(user, iddata) {
    let a = JSON.parse(localStorage.getItem('currentUser'));
    let headers_object = new HttpHeaders().set('Authorization', 'Bearer ' + a.token);
    return this.http.patch(environment.baseUrl + '/location/' + iddata.id, user, { headers: headers_object });
  }

  deleteLocation(id: string) {
    let a = JSON.parse(localStorage.getItem('currentUser'));

    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token);
    return this.http.delete<any>(environment.baseUrl + '/location/' + id, { headers: headers_object });
  }


}