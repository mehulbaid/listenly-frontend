import { Injectable } from '@angular/core';
import { Observable,of  } from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {User} from '../model/user.model';
import {environment} from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class NoteService {
  constructor(private http: HttpClient) { 
    let headers = new HttpHeaders({
      'Content-Type': 'applicaion/json',
      'responseType': 'json'
  });
  let options = {headers:headers};

  }

  getNotes() {
    return this.http.get<User[]>(environment.baseUrl+'/noteList');
  }

  getNotesById(id:any) {
    return this.http.get<User>(environment.baseUrl + '/note/' + id);
  }

  createNotes(user) {
      console.log(user)
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.post(environment.baseUrl+'/add-note', user,{headers:headers_object});
  }

  updateNotes(user,iddata) {
    
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
   return this.http.patch(environment.baseUrl + '/edit-note/' + iddata.id, user,{headers:headers_object});
  }

  deleteNotes(id: string) {
    let a= JSON.parse(localStorage.getItem('currentUser'));
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + a.token)
    return this.http.delete<any>(environment.baseUrl + '/delete-note/' + id,{headers:headers_object});
  }


}