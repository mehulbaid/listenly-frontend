import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private http: HttpClient) { }

  options = {headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })};

  getLanguage() {
    return this.http.get(environment.baseUrl + '/getlanguage', this.options);
  }

  addLanguage(name, code) {
    const a = JSON.parse(localStorage.getItem('currentUser'));
    const authHeader = 'Bearer ' + a.token;
    const options = {headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authHeader
    })};
    const postData = { langName: name, langCode: code };
    return this.http.post(environment.baseUrl + '/addLanguage', postData, options);
  }

  deleteLanguage(id: string) {
    return this.http.delete(environment.baseUrl + '/delete-language/' + id, this.options);
  }


}
