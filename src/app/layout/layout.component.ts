import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuService } from '@nebular/theme';
import { MENU_ITEMS } from '../nav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  sidebarCollapsed = false;

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private router: Router
  ) { }

  navMenu = MENU_ITEMS;

  toggle() {
    this.sidebarService.toggle(true);
    if (this.sidebarCollapsed === true) {
      this.sidebarCollapsed = false;
    } else {
      this.sidebarCollapsed = true;
    }
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['login']);
  }

  ngOnInit() {
    this.sidebarService.onCollapse().subscribe(coll => {
      this.sidebarCollapsed = false;
    });
  }

}
